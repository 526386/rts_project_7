﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WinLoseController : MonoBehaviour
{
    [SerializeField]
    private Base1 m_PlayerBase;

    [SerializeField]
    private Base2 m_EnemyBase;

    [SerializeField]
    private TextMeshProUGUI m_WinLoseText;

    // Start is called before the first frame update
    void Start()
    {
        m_WinLoseText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        CheckBaseHealth();
    }

    private void CheckBaseHealth()
    {
        // Checks here the health of the players building

        if(m_PlayerBase.HpToReturn() <= 0)
        {
            Player2Win();
        }

        if(m_EnemyBase.HpToReturn() <= 0)
        {
            Player1Win();
        }
    }

    private void Player1Win()
    {
        m_WinLoseText.text = "Player 1 wins!";
    }

    private void Player2Win()
    {
        m_WinLoseText.text = "Player 2 wins!";
    }
}
