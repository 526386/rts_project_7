﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField]
    private List<BaseEnemy> m_EnemyToInstantiate = new List<BaseEnemy>();

    [SerializeField]
    private List<Transform> m_InstantiatePoints = new List<Transform>();

    private GameTimer m_GameTimer;

    [SerializeField]
    private float m_MaxTimer;
    private float m_CurTimer;
    
    void Start()
    {
        m_CurTimer = 0;
    }
    
    void Update()
    {
        m_CurTimer += Time.deltaTime;

        TimeChecker();
    }

    private void TimeChecker()
    {
        if(m_CurTimer >= m_MaxTimer)
        {
            // Chooses a point to create a unit

            ChooseInstantiatePoint();

            m_CurTimer = 0;
        }
    }

    private void ChooseInstantiatePoint()
    {
        int RandomPoint = Random.Range(0, m_InstantiatePoints.Count);

        int RandomUnit = Random.Range(0, m_EnemyToInstantiate.Count);

        this.m_EnemyToInstantiate[0].transform.position = m_InstantiatePoints[RandomPoint].transform.position;

        Instantiate(m_EnemyToInstantiate[0]);

        Debug.Log("Enemy has been instantiated");
    }

    //private void AddToUnitList()
    //{
    //    if(m_GameTimer.Timer() >= 300f)
    //    {
    //        GameObject UnitToAdd = (GameObject)Resources.Load("Prefabs/Ayoub/Enemy", typeof(GameObject));
    //    }
    //}
}
