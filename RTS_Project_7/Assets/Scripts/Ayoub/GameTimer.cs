﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameTimer : MonoBehaviour
{
    private float m_Timer;

    [SerializeField]
    private TextMeshProUGUI m_TimerText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_Timer += Time.deltaTime;

        float Minutes = Mathf.Floor(m_Timer / 60);

        float Seconds = Mathf.RoundToInt(m_Timer % 60);

        m_TimerText.text = string.Format("{0:0}:{1:00}", Minutes, Seconds);
    }

    public float Timer()
    {
        return m_Timer;
    }
}
