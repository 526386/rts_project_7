﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField]
    private BaseTeam m_UnitToSpawn;

    private InstantiateUnit m_InstantiateUnit;

    private Resource m_Resource;

    private Transform m_OriginalPosition;

    private void Start()
    {
        m_InstantiateUnit = FindObjectOfType<InstantiateUnit>();

        m_Resource = FindObjectOfType<Resource>();
    }

    public void OnBeginDrag(PointerEventData EventData)
    {
        m_OriginalPosition = transform.parent;

        m_OriginalPosition.SetParent(transform.parent);
    }

    public void OnDrag(PointerEventData EventData)
    {
        transform.position = Input.mousePosition;
    }

    public void OnEndDrag(PointerEventData EventData)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        //m_Pointer.position = Input.mousePosition;

        //List<RaycastResult> result = new List<RaycastResult>();
        //m_GraphicsRayCast.Raycast(m_Pointer, result);

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "Spawn")
            {
                if (m_InstantiateUnit.Unit() == null)
                {
                    m_InstantiateUnit.InstantiatePoint(hit.transform);

                    m_InstantiateUnit.UnitToInstantiate(m_UnitToSpawn);

                    if (m_InstantiateUnit.TransformToReturn() != null)
                    {
                        m_InstantiateUnit.BuyUnit(m_UnitToSpawn.PriceToReturn());
                    }
                }
                else if (m_InstantiateUnit.Unit() != null)
                    {
                    Debug.Log("There is already a unit in here");
                }
            }
        }

        transform.position = m_OriginalPosition.position;
    }
}
