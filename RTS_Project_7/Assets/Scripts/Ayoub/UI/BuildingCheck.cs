﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BuildingCheck : MonoBehaviour
{
    private InstantiateUnit m_InstUnit;

    private UIController m_UIController;

    //private PointerEventData m_Pointer;

    //private GraphicRaycaster m_GraphicsRayCast;

    // Start is called before the first frame update
    void Start()
    {
        m_InstUnit = FindObjectOfType<InstantiateUnit>();

        m_UIController = FindObjectOfType<UIController>();

        //m_Pointer = new PointerEventData(EventSystem.current);

        //m_GraphicsRayCast = FindObjectOfType<GraphicRaycaster>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        //m_Pointer.position = Input.mousePosition;

        //List<RaycastResult> result = new List<RaycastResult>();
        //m_GraphicsRayCast.Raycast(m_Pointer, result);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Spawn")
                {
                    m_InstUnit.InstantiatePoint(hit.transform);
                }

                Debug.Log(hit.transform.name);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            m_UIController.InactiveUI();
        }
    }
}
