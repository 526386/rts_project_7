﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHolder : MonoBehaviour
{
    [SerializeField]
    private GameObject m_UIObject;

    private Text m_UIText;

    // Start is called before the first frame update
    void Start()
    {
        m_UIText = Instantiate(m_UIObject, FindObjectOfType<Canvas>().transform).GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        m_UIText.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, transform.position.y + 1, 0));
    }
}