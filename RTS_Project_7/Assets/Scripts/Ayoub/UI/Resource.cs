﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Resource : MonoBehaviour
{
    [SerializeField]
    private int m_ResourceToAdd;
    public float m_ResourceHolder;

    private float m_ResourceTimer;

    [SerializeField]
    private float m_ResourceTimeToReset;

    [SerializeField]
    private TextMeshProUGUI m_ResourceText;

    [SerializeField]
    private Image m_Image;

    void Update()
    {
        m_ResourceText.text = "Water: " + m_ResourceHolder;

        m_ResourceTimer += Time.deltaTime;

        m_Image.fillAmount = m_ResourceHolder / 100;

        ResourceCheck();

        TimerCheck();

        if (Input.GetKeyDown(KeyCode.Q))
        {
            AddResource();
        }
    }

    private void ResourceCheck()
    {
        if(m_ResourceHolder >= 999)
        {
            m_ResourceHolder = 999;
        }

        if(m_ResourceHolder <= 0)
        {
            m_ResourceHolder = 0;
        }
    }

    private void TimerCheck()
    {
        if(m_ResourceTimer >= m_ResourceTimeToReset)
        {
            AddResource();

            m_ResourceTimer = 0;
        }

        if(m_ResourceTimer < 0)
        {
            m_ResourceTimer = 0;
        }
    }

    public void AddResource()
    {
        m_ResourceHolder += m_ResourceToAdd;
    }

    public void RemoveResource(int resource)
    {
        m_ResourceHolder -= resource;
    }
}
