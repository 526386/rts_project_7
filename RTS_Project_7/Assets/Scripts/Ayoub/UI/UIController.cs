﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GameObject m_BuyUnitsUI;

    private bool m_IsActive;

    // Start is called before the first frame update
    void Start()
    {
        m_BuyUnitsUI.SetActive(false);

        m_IsActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        UICheck();
    }

    private void EnableUI()
    {
        m_BuyUnitsUI.SetActive(true);
    }

    private void DisableUI()
    {
        m_BuyUnitsUI.SetActive(false);
    }

    public void UICheck()
    {
        if (m_IsActive)
        {
            EnableUI();
        }
        else
        {
            DisableUI();
        }
    }

    public bool ActiveUI()
    {
        m_IsActive = true;

        return m_IsActive;
    }

    public bool InactiveUI()
    {
        m_IsActive = false;

        return m_IsActive;
    }

    public GameObject ReturnUIObject()
    {
        return m_BuyUnitsUI.GetComponentInChildren<GameObject>();
    }
}
