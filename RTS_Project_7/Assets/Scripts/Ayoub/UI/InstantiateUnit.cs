﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateUnit : MonoBehaviour
{
    private Transform m_PointToInstantiate;

    private Resource m_Resource;
    
    private BaseTeam m_UnitTiInstantiate;

    private float m_TimeToInstantiate;

    private float m_Timer;

    private void Start()
    {
        m_Resource = FindObjectOfType<Resource>();
    }

    public void BuyUnit(int ResourceToBuy)
    {
        if (m_PointToInstantiate != null && m_UnitTiInstantiate != null)
        {
            if(m_Resource.m_ResourceHolder >= ResourceToBuy)
            {
                m_Resource.RemoveResource(ResourceToBuy);

                TimeToSpawnUnit(m_UnitTiInstantiate.TimeToReturn());

                Debug.Log("Unit " + m_UnitTiInstantiate + " has been bought on " + m_PointToInstantiate);
            }
            else
            {
                Debug.Log("You don't have the money to buy the unit");
            }
        }
        else
        {
            Debug.Log("There is no point to instantiate");
        }
    }

    private void Update()
    {
        TimeChecker();
    }

    public void InstantiatePoint(Transform point)
    {
        if(point == null)
        {
            return;
        }

        m_PointToInstantiate = point;
    }

    public void UnitToInstantiate(BaseTeam Unit)
    {
        if(Unit == null)
        {
            return;
        }

        m_UnitTiInstantiate = Unit;
    }

    public Transform TransformToReturn()
    {
        return m_PointToInstantiate;
    }

    public BaseTeam Unit()
    {
        return m_UnitTiInstantiate;
    }

    public void TimeToSpawnUnit(float time)
    {
        m_Timer = time;
    }

    private void TimeChecker()
    {
        m_Timer--;

        if (m_Timer <= 0)
        {
            m_Timer = 0;

            if (m_UnitTiInstantiate != null)
            {
                m_UnitTiInstantiate.transform.position = m_PointToInstantiate.transform.position;

                Instantiate(m_UnitTiInstantiate);

                m_UnitTiInstantiate = null;
            }
        }
    }
}
