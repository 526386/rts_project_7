﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    // Dit kan veranderen om het te optimaliseren
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Worker")
        {
            CollectedResource resource = other.GetComponent<CollectedResource>();

            if (resource.ReturnBool() == true)
            {
                Resource controller = FindObjectOfType<Resource>();

                //controller.AddResources();

                //resource.EmptyResource();
            }
        }

        Debug.Log(other.name);
    }
}
