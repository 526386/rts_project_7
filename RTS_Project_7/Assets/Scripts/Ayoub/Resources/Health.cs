﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class Health : MonoBehaviour
{
    [SerializeField]
    private float m_MaxHealth;
    private float m_CurrentHealth;

    [SerializeField]
    private List<Sprite> m_ImageList = new List<Sprite>();
    
    [SerializeField]
    private Image m_HealthBar;

    private void Start()
    {
        m_CurrentHealth = m_MaxHealth;

        m_HealthBar.sprite = m_ImageList[0];

        m_HealthBar = Instantiate(m_HealthBar, FindObjectOfType<Canvas>().transform).GetComponent<Image>();
    }

    private void Update()
    {
        m_HealthBar.transform.position = Camera.main.WorldToScreenPoint(transform.position + new Vector3(0, transform.position.y + 1, 0));

        Debug.Log(m_CurrentHealth);

        if (Input.GetKeyDown(KeyCode.Q))
        {
            TakingDamage(10);
        }

        if(m_CurrentHealth != m_MaxHealth)
        {
            m_HealthBar.sprite = m_ImageList[1];
        }
    }

    private void TakingDamage(float damage)
    {
        m_CurrentHealth -= damage;
    }
}

