﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectedResource : MonoBehaviour
{
    [SerializeField]
    private bool m_HasResource = false;

    public bool HasCollectedResource()
    {
        m_HasResource = true;

        return m_HasResource;
    }

    public bool EmptyResource()
    {
        m_HasResource = false;

        return m_HasResource;
    }

    public bool ReturnBool()
    {
        return m_HasResource;
    }
}
