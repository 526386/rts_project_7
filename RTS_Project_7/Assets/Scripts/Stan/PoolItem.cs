﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PoolItem : MonoBehaviour
{

    ObjectPool m_Pool;

    public ObjectPool Pool
    {
        set{ m_Pool = value; }
    }

    protected abstract void Restart();

	public virtual void Init(Vector3 positiion, Quaternion rotation, Transform parent)
    {
        Restart();
        gameObject.SetActive(true);
        transform.position = positiion;
        transform.rotation = rotation;
        transform.parent = parent;
    }

    public virtual void ReturnToPool()
    {
        m_Pool.AddItem(this);
    }
}
