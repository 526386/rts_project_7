﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base2 : MonoBehaviour
{
    public float _HP;
   
    void Start()
    {
        _HP = 100;
    }

    
    public virtual void TakeDMG(float _DMG)
    {
        //Warrior Damage
        _HP -= _DMG;
        Debug.Log("Yeh i took dmg");
    }

    public float HpToReturn()
    {
        return _HP;
    }
}
