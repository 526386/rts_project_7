﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Base1 : MonoBehaviour
{
    [SerializeField] private float _HP;

    void Start()
    {
        _HP = 100f;   
    }

    public virtual void Team1TakeDMG(float _DMG)
    {
        //Warrior Damage
        _HP -= _DMG;
        Debug.Log("Yeh i took dmg");
    }

    public float HpToReturn()
    {
        return _HP;
    }
}
