﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private GameObject m_PoolObject;

    [SerializeField] private int m_PoolSize;
    
    List<PoolItem> m_Pool;
    
	void Start ()
    {
        m_Pool = new List<PoolItem>();

        GameObject m_Temp;
        for (int i = 0; i < m_PoolSize; i++)
        {
            m_Temp = Instantiate(m_PoolObject);
            m_Temp.transform.position = transform.position;
            AddItem(m_Temp.GetComponent<PoolItem>());
            m_Pool[i].Pool = this;
        }
	}

    public void AddItem(PoolItem item)
    {
        m_Pool.Add(item);

        item.transform.parent = transform;
        item.gameObject.SetActive(false);
    }

    public GameObject InstantiateObject(Vector3 positiion, Quaternion rotation, Transform parent = null)
    {
        Debug.Log("I get here");
        if(m_Pool.Count <= 0)
        {
            Debug.Log("Its null dibshit");
            return null;
        }
        
        m_Pool[0].Init(positiion, rotation, parent);
        GameObject item = m_Pool[0].gameObject;
        m_Pool.RemoveAt(0);
        return item;
    }
}
