﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warrior : BaseTeam
{
    [SerializeField] private Transform _Warrior;
    [SerializeField] private float _AttRange;
   
    private void OnTriggerEnter(Collider other)
    {
        //Spot enemies
        if(other.CompareTag("Team 2"))
        {
            AttackEnemy();
        }
        if (other.CompareTag("Buildings 2"))
        {
            AttackBuilding();
        }
    }

    public override void AttackEnemy()
    {
        if (base._AttTimerWar <= 0)
        {
            //Damage enemy
            Collider[] _EnemysToDMG = Physics.OverlapSphere(_Warrior.position, _AttRange, _EnemyMask);
            for (int i = 0; i < _EnemysToDMG.Length; i++)
            {
                _EnemysToDMG[i].GetComponent<BaseEnemy>().TakeDMG(base._WarriorDamage);
                base._AttTimerWar = 0.5f;
            }
        }
    }

    public override void AttackBuilding()
    {
        if (base._AttTimerWar <= 0)
        {
            Collider[] _BuildingsToDMG = Physics.OverlapSphere(_Warrior.position, _AttRange, _EnemyMask);
            for (int i = 0; i < _BuildingsToDMG.Length; i++)
            {
                _BuildingsToDMG[i].GetComponent<Base2>().TakeDMG(base._WarriorDamage);
                base._AttTimerWar = 0.5f;   
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        //Debugging hits
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_Warrior.position, _AttRange);
    }
}
