﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gatherer : MonoBehaviour
{
    public float _Speed;
    private int _HP;

    private void Update()
    {
        transform.position += transform.right * _Speed * Time.deltaTime;
    }
}
