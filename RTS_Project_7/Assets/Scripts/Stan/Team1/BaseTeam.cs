﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTeam : MonoBehaviour
{
    [SerializeField] private float _Speed;

    [SerializeField] LayerMask _BuildingMask;
    [SerializeField] LayerMask _TeamMask;
    [SerializeField] LayerMask _GathererMask;
    public LayerMask _EnemyMask;

    public int _Range;
    [SerializeField] private int _Cost;
    [SerializeField] private float _SpawnTime;

    //Generic
    private bool _AttCountdown;
    public bool _Move;
    public float _HP;
    public float _SecondRange;
    //Archer
    public float _ArrowDMG;
    public float _AttTimer = 2;
    //Warrior
    public float _AttTimerWar = 0.5f;
    public float _WarriorDamage = 10f;
    //SpellCaster
    public float _MageAttTimer;
    public float _MageDMG;
    

    void Start()
    {
        _Move = true;
        _AttCountdown = false;
    }

    public virtual void Update()
    {
        //Check for enemys
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), _Range, _EnemyMask))
        {
            AttackEnemy();
            _Move = false;
            _AttCountdown = true;
        }
        //Check for enemy gatherers
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), _Range, _GathererMask))
        {
            AttackEnemy();
            _Move = false;
            _AttCountdown = true;
        }
        //Check for buildings
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), _Range, _BuildingMask))
        {
            AttackBuilding();
            _Move = false;
            _AttCountdown = true;
        }
        //Check for teammates
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), _SecondRange, _TeamMask))
        {
            _Move = false;
        }
        //Check for free path
        else
        {
            _Move = true;
        } 
        
        //Movement
        if (_Move)
        {
            transform.position += transform.right * _Speed * Time.deltaTime;
        }

        //Death
        if (_HP <= 0)
        {
            this.gameObject.SetActive(false);
        }

        //Attacking
        if (_AttCountdown)
        {
            _AttTimer -= Time.deltaTime;
            _AttTimerWar -= Time.deltaTime;
            _MageAttTimer -= Time.deltaTime;
        }

        if(_AttTimer <= -0.1f)
        {
            _AttTimer = 2;
        }
        if(_AttTimerWar <= -0.3f)
        {
            _AttTimerWar = 0.5f;
        }
        if(_MageAttTimer <= 0f)
        {
            _MageAttTimer = 2;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Arrow Damage
        if (other.CompareTag("Ranged Team 2"))
        {
            _HP -= _ArrowDMG;
        }
        if (other.CompareTag("Magic Team 2"))
        {
            _HP -= _MageDMG;
        }
    }

    public virtual void Team1TakeDMG(float _DMG)
    {
        //Warrior Damage
        _HP -= _DMG;
    }

    public int PriceToReturn()
    {
        return _Cost;
    }
    public float TimeToReturn()
    {
        return _SpawnTime;
    }
    public virtual void AttackEnemy()
    {
        
    }
    public virtual void AttackBuilding()
    {
        
    }
}
