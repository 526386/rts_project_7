﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Water : MonoBehaviour
{
    [SerializeField] private Gatherer _Gatherer1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Team 1 Gatherer"))
        {
            _Gatherer1._Speed = _Gatherer1._Speed * -1;
        }
    }
}
