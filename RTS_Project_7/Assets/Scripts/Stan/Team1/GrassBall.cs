﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassBall : MonoBehaviour
{
    [SerializeField] private float _ProjectileSpeed;
    [SerializeField] private float _DeathTimer;

    void Update()
    {
        transform.Translate(Vector3.right * Time.deltaTime * _ProjectileSpeed);
        _DeathTimer -= Time.deltaTime;

        if(_DeathTimer <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
