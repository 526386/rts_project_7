﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Returned : MonoBehaviour
{
    [SerializeField] private Gatherer _Gatherer1;
    [SerializeField] private int _Water;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Team 1 Gatherer"))
        {
            _Gatherer1._Speed = _Gatherer1._Speed * -1;
            _Water += 10;
        }
    }
}
