﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : BaseTeam
{
    [SerializeField] private GameObject _GrassBall;
    public float _TimeBetweenShots = 0.4f;
    private bool _Countdown = false;

    public override void AttackEnemy()
    {
        if(base._MageAttTimer <= 0.1f)
        {
            _Countdown = true;
        }
        if(_Countdown)
        {
            _TimeBetweenShots -= Time.deltaTime;
        }
        if(_TimeBetweenShots <= 0f)
        {
            Debug.Log("Shooting");
            GameObject.Instantiate(_GrassBall, this.transform.position, Quaternion.identity);
            _Countdown = false;
            _TimeBetweenShots = 0.4f;

        }
    }

    public override void AttackBuilding()
    {
        AttackEnemy();
    }
}
