﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : PoolItem
{
    private float m_DeathTimer;

    protected override void Restart()
    {
        m_DeathTimer = 0.0f;
    }
    
    void Update ()
    {
        transform.Translate(Vector3.right * Time.deltaTime * 10.0f);

        m_DeathTimer += Time.deltaTime;
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Team 2"))
        {
            Destroy(this.gameObject);
        }
    }
}
