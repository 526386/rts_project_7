﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : PoolItem
{
    private float _DeathTimer;

    protected override void Restart()
    {
        _DeathTimer = 0.0f;
    }

    void Update()
    {
        transform.Translate(Vector3.left * Time.deltaTime * 10.0f);

        _DeathTimer += Time.deltaTime;
        if(_DeathTimer >= 5)
        {
            ReturnToPool();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Team 1"))
        {
            Destroy(this.gameObject);
        }
    }
}
