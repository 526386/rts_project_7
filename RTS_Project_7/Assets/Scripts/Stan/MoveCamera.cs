﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    [SerializeField] private float _Speed;

    private Vector3 _Mousepos;
    private int _Width;

    private void Start()
    {
        _Width = Screen.width;
    }


    private void Update()
    {   
        _Mousepos.x = Input.mousePosition.x;

        if (_Mousepos.x >= _Width - 100)
        {
            transform.position += transform.right * _Speed * Time.deltaTime;
        }

        if (_Mousepos.x <= 100)
        {
            transform.position -= transform.right * _Speed * Time.deltaTime;
        }

        if(this.gameObject.transform.position.x <= -37)
        {
            this.gameObject.transform.position = new Vector3(-37, 0, -10);
        }

        if (this.gameObject.transform.position.x >= 37)
        {
            this.gameObject.transform.position = new Vector3(37, 0, -10);
        }
    }
}
