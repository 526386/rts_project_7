﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour
{
    [SerializeField] private float _Speed;

    public LayerMask _Team2EnemyMask;
    [SerializeField] LayerMask _Team2BuildingMask;
    [SerializeField] LayerMask _Team2TeamMask;
    [SerializeField] LayerMask _GathererMask;

    public int _Range;
    //Generic
    private bool _AttCountdown;
    public bool _Move;
    public float _HP;
    public float _SecondRange;
    //Archer
    public float _ArrowDMG;
    public float _AttackTimer;
    //Warrior
    public float _AttTimerWar = 0.5f;
    public float _WarriorDMG = 10f;
    //SpellCaster
    public float _MageAttTimer;
    public float _MageDMG;
   

    void Start()
    {
        _Move = true;
         _AttCountdown = false;
    }

    public virtual void Update()
    {
        //Check for enemys
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), _Range, _Team2EnemyMask))
        {
            AttackEnemy();
            _Move = false;
            _AttCountdown = true;
        }
        ////Check for enemy gatherers
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), _Range, _GathererMask))
        {
            AttackEnemy();
            _Move = false;
            _AttCountdown = true;
        }
        //Check for buildings
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), _Range, _Team2BuildingMask))
        {
            AttackBuilding();
            _Move = false;
            _AttCountdown = true;
        }
        //Check for teammates
        else if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), _SecondRange, _Team2TeamMask))
        {
            _Move = false;
        }
        //Check for free path
        else
        {
            _Move = true;
        }

        //Movement
        if (_Move)
        {
            transform.position -= transform.right * _Speed * Time.deltaTime;
        }

        //Death
        if(_HP <= 0)
        {
            this.gameObject.SetActive(false);
        }

        //Attacking
        if (_AttCountdown)
        {
            _AttackTimer -= Time.deltaTime;
            _AttTimerWar -= Time.deltaTime;
            _MageAttTimer -= Time.deltaTime;
        }

        if (_AttackTimer <= 0f)
        {
            _AttackTimer = 2;
        }
        if (_AttTimerWar <= -0.3f)
        {
            _AttTimerWar = 0.5f;
        }
        if (_MageAttTimer <= 0f)
        {
            _MageAttTimer = 2;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Arrow Damage
        if (other.CompareTag("Ranged Team 1"))
        {
            _HP -= _ArrowDMG;
        }
        if (other.CompareTag("Magic Team 1"))
        {
            _HP -= _MageDMG;
        }
    }


    public virtual void TakeDMG(float _DMG)
    {
        //Warrior Damage
        _HP -= _DMG;
    }

    public virtual void AttackEnemy()
    {

    }
    public virtual void AttackBuilding()
    {

    }
}
