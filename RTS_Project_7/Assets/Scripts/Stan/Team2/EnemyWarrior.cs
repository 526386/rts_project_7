﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWarrior : BaseEnemy
{
    [SerializeField] private Transform _Warrior;
    [SerializeField] private float _AttRange;

    private void OnTriggerEnter(Collider other)
    {
        //Spot enemies
        if (other.CompareTag("Team 1"))
        {
            AttackEnemy();
        }
        if (other.CompareTag("Buildings 1"))
        {
            AttackBuilding();
        }
    }

    public override void AttackEnemy()
    {
        if (base._AttTimerWar <= 0)
        {
            //Damage enemy
            Collider[] _EnemysToDMG = Physics.OverlapSphere(_Warrior.position, _AttRange, _Team2EnemyMask);
            for (int i = 0; i < _EnemysToDMG.Length; i++)
            {
                _EnemysToDMG[i].GetComponent<BaseTeam>().Team1TakeDMG(base._WarriorDMG);
                base._AttTimerWar = 0.5f;
            }
        }
    }

    public override void AttackBuilding()
    {
        if(base._AttTimerWar <= 0)
        {
            Collider[] _EnemysToDMG = Physics.OverlapSphere(_Warrior.position, _AttRange, _Team2EnemyMask);
            for (int i = 0; i < _EnemysToDMG.Length; i++)
            {
                _EnemysToDMG[i].GetComponent<Base1>().Team1TakeDMG(base._WarriorDMG);
                base._AttTimerWar = 0.5f;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        //Debugging hits
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_Warrior.position, _AttRange);
    }
}
