﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRanged : BaseEnemy
{
    [SerializeField] private GameObject _Arrow;
    [SerializeField] private Transform _Origin;

    private float _TimeBetweenShots = 0.5f;
    private bool _CountDown = false;


    public ObjectPool _Pool;

    public override void AttackEnemy()
    {
        if(base._AttackTimer <= 0.1f)
        {
            _CountDown = true;
        }

        if (_CountDown)
        {
            _TimeBetweenShots -= Time.deltaTime;
        }

        if (_TimeBetweenShots <= 0)
        {
            _TimeBetweenShots = 0.5f;
            Debug.Log("Shoo");
            //Spawn Arrows
            Instantiate(_Arrow, _Origin.position, Quaternion.identity);
            _CountDown = false;
        }
    }
    public override void AttackBuilding()
    {
        base.AttackBuilding();
    }
}
