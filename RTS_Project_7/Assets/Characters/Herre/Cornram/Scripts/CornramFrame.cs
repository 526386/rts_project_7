﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CornramFrame : MonoBehaviour
{
   // public int Speed;
   // public GameObject _Particle;
    public float _Amplitude = 0.05f;
    public float _Frequency = 3f;
    private Vector3 PositionOffset;
    private Vector3 TempPos;
    // Use this for initialization
    void Start()
    {
        PositionOffset = transform.position;
    }

    void
        Update()
    {
       // transform.Rotate(Speed * Time.deltaTime, Speed * Time.deltaTime, Speed * Time.deltaTime);
        TempPos.y = PositionOffset.y;
        TempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * _Frequency) * _Amplitude;
        transform.position = new Vector3(transform.position.x, TempPos.y, transform.position.z);
    }
   //     Update is called once per frame
   // void OnTriggerEnter()
   // {
   //     Destroy(_Particle);
   //     Destroy(gameObject);
   // }
}
